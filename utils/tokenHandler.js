
/**
 * Checks if a valid token exists in the localStorage and if it is not expired.
 * @returns {boolean} True if a valid, non-expired token exists; otherwise, false.
 */
export function hasValidToken() {
    // Retrieve the token from localStorage
    const token = localStorage.getItem('token');

    // Check if the token exists
    if (!token) {
        return false;
    }

    try {
        // Decode the base64 payload of the token
        const tokenPayload = JSON.parse(atob(token.split('.')[1]));

        // Extract the expiration timestamp and convert it to milliseconds
        const expirationTimestamp = tokenPayload.exp * 1000;

        // Check if the token is expired
        if (Date.now() > expirationTimestamp) {
            // Token is expired, remove it from localStorage
            localStorage.removeItem('token');
            return false;
        }


        // Token is valid and not expired
        return true;
    } catch (error) {
        // An error occurred while parsing or validating the token
        console.error('Error parsing or validating token:', error);

        // Remove the invalid token from localStorage
        localStorage.removeItem('token');

        // Token is not valid
        return false;
    }

}

/**
 * Checks if the current user is a manager based on the provided token.
 *
 * @returns {boolean} True if the user is a manager, false otherwise.
 */
export function isManager() {
    if (process.client) {
        // Retrieve the token from localStorage
        const token = localStorage.getItem('token');

        // Check if the token exists
        if (!token) {
            return false;
        }

        try {
            // Decode the base64 payload of the token
            const tokenPayload = JSON.parse(atob(token.split('.')[1]));

            // Check the user type in the token payload
            const isManager = tokenPayload.is_manager;

            // Return true if the user is a manager, false otherwise
            return isManager || false;
        } catch (error) {
            // An error occurred while parsing or validating the token
            console.error('Error parsing or validating token:', error);

            // Token is not valid
            return false;
        }
    }

    // If not in the client environment, return false
    return false;
}

