// utils/api.js
import { fetchApiData } from './apiHandler';

/**
 * Sends an OTP code to the provided email or mobile number.
 *
 * @param {string} emailMobile - The email or mobile number to send OTP to.
 * @returns {Promise} - A promise that resolves when the API request is completed.
 */
export async function sendOtpCode(emailMobile) {
    return fetchApiData('/users/auth/send/otp', 'post', { user_name: emailMobile });
}

/**
 * Resends an OTP code to the provided email or mobile number.
 *
 * @param {string} emailMobile - The email or mobile number to resend OTP to.
 * @returns {Promise} - A promise that resolves when the API request is completed.
 */
export async function resendOtpCode(emailMobile) {
    return fetchApiData('/users/auth/send/otp', 'post', { user_name: emailMobile });
}

/**
 * Logs out the user from the authentication session.
 *
 * @returns {Promise} - A promise that resolves when the API request is completed.
 */
export async function logout() {
    return fetchApiData('/users/auth/logout', 'post');
}
