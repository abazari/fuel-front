
const API_BASE_URL = process.env.API_BASE_URL === undefined ? 'http://localhost:8000' : process.env.API_BASE_URL;

export function getImageUrl(file) {
    if (file) {
        // Assuming image.file contains a relative path (e.g., /images/image.jpg)
        return `${API_BASE_URL}${file}`;
    }


    return '/images/default_image.png'
}

export function getLinkParameter(address,parameter,parameter1=null) {
    if (parameter1){
        return `/${address}/${parameter}/${parameter1}`

    }

    return `/${address}/${parameter}`
}

export function getLinkParameterAnotherType(address,nameParameter,parameter,nameParameter1=null,parameter1=null) {
    if (parameter1){
        return `/${address}?${nameParameter}=${parameter}&&${nameParameter1}=${parameter1}`

    }

    return `/${address}?${nameParameter}=${parameter}`
}

export function arrayToString(arr) {
    return arr.join(', ');
}

export function isNotToken() {
    if (process.client) {
        return !localStorage.getItem('token')
    }
    return false
}

export function detectDeviceType() {
    return window.matchMedia('(min-width: 1025px)').matches;
}