import axios from "axios";
import {ref} from "vue";
export const errorValidationsApi = ref(null);
/**
 * Fetch data from the API.
 * @param {string} url - The URL to fetch data from.
 * @param {string} method - The HTTP method (get, post, put, delete).
 * @param {object} attributes - Data to send in the request.
 * @returns {object} - The API response.
 */
export async function fetchApiData(url, method = 'get', attributes = {}) {
    try {
        const headers = {};

        // if (['post', 'get', 'put', 'delete'].includes(method.toLowerCase())) {
        //     response = await axios({ method, url, headers, data: attributes });
        // } else {
        //     throw new Error(`${method} is not a valid HTTP method`);
        // }
        // Choose the appropriate HTTP method
        let response;
        if (method === 'post') {
            response = await axios.post(url, attributes);
        } else {
            response = await axios.get(url, { params: attributes });
        }

        const responseData = response.data;

        if (responseData.errors) {
            console.log(`Error in API response: ${responseData.message}, status_error: ${responseData.status}`);
            return {
                error: true,
                status: responseData.status,
                result: {},
                message: responseData.message
            };
        }

        return {
            error: false,
            status: responseData.status,
            result: responseData.result,
            message: responseData.message
        };
    } catch (error) {
        return {
            error: true,
            result: {},
            status: error.response?.status || 500,
            message: error.response ? error.response.data : error.message
        };
    }
}

/**
 * Handles API response errors by updating error validations, displaying a notification,
 * and setting loading state.
 *
 * @param {Object} response - The API response object.
 * @param {Ref<UnwrapRef<null>>} errorValidations - A Vue ref to store error validations.
 * @param {Ref<UnwrapRef<null>>} notification - A Vue ref to store notification details.
 * @param {Ref} notificationVisible - A Vue ref to control the visibility of the notification.
 * @param {Ref} isLoading - A Vue ref to control loading state.
 * @param {number} timeoutDuration - The duration in milliseconds to hide the notification.
 */
export async function handleApiError(response, errorValidations, notification, notificationVisible, isLoading, timeoutDuration) {

    if (response.error && response.status == 422) {
        errorValidations.value = response.message.errors;
    }
  /*  else if (response.message.response && response.message.response.status === 422 ) {
        // Update error validations with API validation errors
        errorValidations.value = response.message.response.data.errors;
    }*/
    else {
        // Set notification details for API error
        notification.value = {
            message: response.message,
            type: 'error'
        };

        // Display the notification
        notificationVisible.value = true;

        // Hide the notification after the specified duration
        setTimeout(() => {
            notificationVisible.value = false;
        }, timeoutDuration);
    }

    // Set loading state to false
    isLoading.value = false;
}



