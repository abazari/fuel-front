import {hasValidToken} from "~/utils/tokenHandler";

export default defineNuxtRouteMiddleware((to, from) => {
    if (process.client) {
        if (!hasValidToken()) {
            return navigateTo('/auth/login');
        }

    }
});
