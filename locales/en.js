export default {
    welcome: 'Welcome',
    home: 'Home',
    about: 'About',
    app_name: 'Fuel',
    not_access: 'You lack the necessary permissions to access this section.',
    information: {
        noDataMessage: "Oops! No information available right now.",
    },
    explain_for_client_order: "Please enter your details in the form below for fueling, and submit the information so that operators can register your order and proceed with the loading process.",
    // hello: 'Hello, {name}!'
    mode_page: {
        toggle_mode: 'Toggle Mode',
        light: 'Light',
        dark: 'dark',
    },
    menu: {
        home: 'Home',
        home_user: 'Home User',
        orders: 'Orders',
        jobs: 'Jobs',
        account: 'Account',
    },
    pages: {
        index: {
            title: 'Home Page'
        },
        general:{
            nothing_data: 'There is no show data'
        },
        post : {
            show : 'Show',
            my: 'My Post',
            all: 'All Posts',
            insert: "Insert Post",
            alert: {
                success: 'Your post has been successfully submitted',
                cancel: 'Your request has been canceled',
            }
        },
        login: {
            title: 'Login to Besparesh'
        },
        order: {
            title_order: 'Orders',
            title: 'Your Order',
            login_text: 'You are not logged in, so enter your name and mobile number',
            help_file_upload: "You can upload multiple files and attach them to your order. The allowed number of uploads is Number {number}.",
            title_details: "Title Details {name}",
            order_details: {
                amount_title: '{orderable} Amount: {amount}'
            }

        },
        jobs: {
            ad: {
                title: 'Recommended Jobs'
            },
            registered: {
                recent: 'Most Recent Jobs'
            },
            title_details: 'Jobs Details'
        },
        errors: {
            code: 'Error with code {code}'
        },
        file: {
            title: 'Uploaded files'
        }
    },
    post_slider: {
        not_display_image: 'No images to display.',
        show: 'Show'
    },

    auth: {
        enter_sent_code: 'Enter the sent code',
        login_app: 'Login to Fuel',
        password_verification: 'One-time password verification',
        verification_code: 'Enter Verification code',
        not_login: 'You are not logged in. To view {section}, you must log in.'
    },
    register_for_buy_fuel: 'Register to buy fuel',
    create_order: 'Create Order',
    create_tenant: 'Create Tenant',
    create_delivery_truck: 'Create Delivery Truck',
    /* Custom Attributes */
    attributes: {
        login: 'Sign in',
        logout: 'Logout',
        welcome: 'Welcome back!',
        send_code: 'Send Code',
        resend_code: 'Resend Code',
        load_image: 'Load Image',
        submit: 'Submit',
        accept: 'َAccept',
        tags: 'Tags',
        context: 'Context',
        status: 'Status',
        public: 'Public',
        private: 'Private',
        title: 'Title',
        type_post: 'Type Post',
        payment: 'Payment',
        code_factor: 'Invoice Code',
        amount_total: 'Amount Total',
        invoice_date: 'Invoice Date',
        payment_status: 'Payment Status',
        amount: 'Amount',
        details: 'Details',
        description: 'Description',
        order_time: 'Order time',
        order_count: 'Order Count',
        order_amount: 'Order Amount',
        orders_list: 'Orders List',
        delivery_time: 'Delivery time',
        jobs_name: 'Jobs name',
        create: "Create",
        create_order: "Create Order",
        show: 'Show',
        order: 'Order',
        recent_orders: 'Recent Orders',
        name: 'Name',
        mobile: 'Mobile',
        type: 'Type',
        quantity: 'Quantity',
        service: 'Service',
        product: 'Product',
        upload: 'Upload',
        orders: 'Orders',
        invoices: 'Invoice',
        transactions: 'Transactions',
        download: 'Download',
        order_details: 'Order Details',
        invoice_total: 'Invoice Total',
        payments: 'Payments',
        tax: 'Tax',
        discount: 'Discount',
        state: 'State',
        tracking_code: 'Tracking Code',
        payment_date: 'Payment Date',
        add_details: 'Add Details',
        add_new_orders: 'Add New Orders',
        actions: 'Action',
        profile: 'Profile',
        follower: 'Follower',
        following: 'Following',
        tickets: 'Tickets',
        client: 'Client',
        tenant: 'Tenant',
        model: 'Model',
        license_plate: 'License Plate',
        tenant_name: 'Tenant Name',
        domain: 'Domain',
        email: 'Email',
        truck_name: 'Truck Name',
        client_name: 'Client Name',
        password: 'Password',
        confirm_Password: 'Confirm Password',
        delivery_truck: 'Delivery Truck',
        address: 'Address',
        user: 'User',
    },
    select: {
        displays: {
            follow: 'Page Followers',
            random_alfa: 'Random Post on Home Page 1',
            random_beta: 'Random Post on Home Page 2',
            vip: 'VIP Post',
        }
    }

};