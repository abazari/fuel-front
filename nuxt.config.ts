// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    css: [
        'bootstrap/dist/css/bootstrap.css',
        'vuetify/lib/styles/main.sass',
        'assets/css/style.css',
    ],
    buildModules: [
        'plugins/vuetify',
        'plugins/vuetify-fas',
        'plugins/i18n',
        'plugins/axios',
        'plugins/fontawesome',
        // 'plugins/font-direction.js',
        '@nuxtjs/axios',
        // '~/plugins/runtimeConfig.js',
    ],
    build: {
        transpile: ['vuetify'],
    },
    vite: {
        define: {
            'process.env.DEBUG': false,
        },
        // Other Vite options
        server: {
            // You can also use an absolute path if needed: `/absolute/path/to/assets`
            serveStatic: ['/fonts'], // Add the font directory to be served by Vite
        },
    },
    app: {
        head: {
            title: "Fuel",
            // viewport: "width=device-width, initial-scale=1",
            // charset: "utf-8",
            meta: [
                {name: 'description', content: 'Description my app'},
                {name: 'viewport', content: 'width=device-width, initial-scale=1'},
                {charset: 'utf-8'}
            ],
            script: [
                // {src: "aaaaaaaaaaaa"}
            ],
            style: [
                // {children: ':root {color :red}', type: 'text/css'}
            ],
            link: [
                // {rel: 'stylesheet', href: "bbbbbbbbbbb"}
            ],
            noscript: [
                // {children: 'asdasd'}
            ],
            // bodyAttrs: {
            //   class: 'dark-mode',
            // },
        },
    },
    runtimeConfig: {
        // API_BASE_URL: process.env.API_BASE_URL || 'http://127.0.0.1:8000',
        public: {
            baseURL: process.env.API_BASE_URL,
            alertTimeoutDuration: process.env.TIMEOUT_DURATION_ALERT,
            singleLogo: '/images/logo.png',
            api: {
                login: 'users/auth/login',
                order: {
                    index: 'users/orders',
                    store: 'users/orders',
                },
                client: {
                    select: 'users/clients/select',
                    store: 'users/clients',
                },
                deliveryTruck: {
                    select: 'users/delivery-trucks/select',
                    index: 'users/delivery-trucks',
                    store: 'users/delivery-trucks',
                    detail: 'users/delivery-trucks/details',
                },
                tenant: {
                    select: 'users/tenants/select',
                    index: 'users/tenants',
                    store: 'users/tenants',
                }
            },
            url: {
                index: '/',
                order: {
                    index: '/order/show',
                    create: '/order/create',
                },
                deliveryTruck: {
                    index: '/truck/show',
                    create: '/truck/create',
                    details: '/truck/details',
                },
                tenant: {
                    index: '/tenant/show',
                    create: '/tenant/create',
                },
                // client: '/client/show',
                auth: {
                    login: '/auth/login'
                }
            },

        }


    }
})
