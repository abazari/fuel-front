import { createI18n } from 'vue-i18n'
import en from "~/locales/en";

export default defineNuxtPlugin(({ vueApp }) => {
    const i18n = createI18n({
        legacy: false,
        globalInjection: true,
        locale: 'en',
        messages: {
            en: {
                ...en,
                direction: 'ltr' // Set the direction to LTR for English language
            }
        }
    })

    vueApp.use(i18n)
})
