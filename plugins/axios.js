import axios from 'axios';

export default defineNuxtPlugin(() => {
    axios.defaults.baseURL = 'http://127.0.0.1:9000/api/v1/'; // Set your base URL here

    // Set default headers
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Accept'] = 'application/json';

    // Set default timeout (in milliseconds)
    axios.defaults.timeout = 5000;

    // Add an interceptor for request configuration
    axios.interceptors.request.use((config) => {
        const token = localStorage.getItem('token');

        if (token && token !== '') {
            config.headers['Authorization'] = `Bearer ${token.replace(/^"(.*)"$/, '$1')}`;
        }

        return config;
    });

    // Add an interceptor for response handling
    axios.interceptors.response.use((response) => {
        // Modify response data if needed
        // response.data = processResponse(response.data);
        return response;
    });

    // Add an interceptor for error handling
    axios.interceptors.response.use(undefined, (error) => {
        // Handle errors globally (e.g., show error toast, redirect to error page)
        console.error('Request error:', error);
        return Promise.reject(error);
    });
});
