// plugins/vuetify.js
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import { mdi } from 'vuetify/iconsets/mdi';
import { md } from 'vuetify/iconsets/md';
import { mdiAccount } from '@mdi/js';
import { fa } from 'vuetify/iconsets/fa';
import { VBottomSheet } from "vuetify/labs/VBottomSheet";
import { VDatePicker } from 'vuetify/labs/VDatePicker';

import '@mdi/font/css/materialdesignicons.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import {
    VDataTable,
    VDataTableServer,
    VDataTableVirtual,
} from "vuetify/labs/VDataTable";
import {VuetifyDateAdapter} from "vuetify/labs/date/adapters/vuetify";


export default defineNuxtPlugin(nuxtApp => {
    const vuetify = createVuetify({
        ssr: true,
            components: {
                VDataTable,
                VDataTableServer,
                VDataTableVirtual,
                VDatePicker,
                VBottomSheet,
                ...components
            },
        directives,
        date: {
            adapter: VuetifyDateAdapter,
            // adapter: DateFnsAdapter,
            // locale: {
            //     en: enUS,
            //     fa: faIR,
            // },
        },

        icons: {
            defaultSet: 'mdi',
            iconfont: 'mdi',
            values: {
                account: mdiAccount,
            },
            sets: {
                mdi,
                md,
                fa,
            },
        },
        theme: {
            defaultTheme: 'dark',
        }

    })

    nuxtApp.vueApp.use(vuetify)

})

